"""
q01.py
8/18/14
Chapter 2, Question 1 in Cracking The Coding Interview
Write code to remove duplicates from an unsorted linked list.
FOLLOW UP
How would you solve this problem if a temporary buffer is not allowed?
"""
from linked_list import Node

__author__ = 'Scott'
def remove_duplicates(head):
    """
    Remove duplicate elements from the linked list pointed to by head
    :param head: head linked_list.Node of linked list
    :return: none, but side effect is modification of list
    """

    curr_node = head
    seen_head = Node(head.data)

    while curr_node.next_node:
        seen = False
        prev_node = curr_node
        curr_node = curr_node.next_node
        curr_data = curr_node.data
        curr_seen = seen_head
        prev_seen = None

        while curr_seen:
            if curr_seen.data == curr_data:
                seen = True
                break
            elif curr_seen.data > curr_data:
                new_seen_node = Node(curr_data)
                new_seen_node.next_node = curr_seen

                if prev_seen: prev_seen.next_node = new_seen_node
                else: seen_head = new_seen_node

                seen = True
                break
            else:
                prev_seen = curr_seen
                curr_seen = curr_seen.next_node

        if seen:
            prev_node.next_node = curr_node.next_node





