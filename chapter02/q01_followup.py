"""
q01_followup.py
8/18/14
Chapter 2, Question 1 in Cracking The Coding Interview
Write code to remove duplicates from an unsorted linked list.
FOLLOW UP
How would you solve this problem if a temporary buffer is not allowed?
"""
from linked_list import Node

__author__ = 'Scott'

def remove_duplicates(head, seen_head=None):
    """
    Remove duplicate elements from the linked list pointed to by head
    :param head: head linked_list.Node of linked list
    :param seen_head: linked list of nodes already seen. becomes final list
    at end
    :return: none, but side effect is modification of list
    """

    if head is None:        # base case
        head = seen_head
        return

    prev_seen = None
    seen_node = seen_head

    while seen_node is not None:
        if head.data < seen_node.data:
            # This means everything to my right is greater than head.data
            # and everything to my left is less than head.data
            # So i have the spot where I need to insert head
            next_head = head.next_node

            if not prev_seen:
                # insert at beginning
                head.next_node = seen_head
                seen_head = head
            else:
                prev_seen.next_node = head
                head.next_node = seen_node

            # and then recurse
            remove_duplicates(next_head, seen_head)
            return

        elif head.data == seen_node.data:
            # it's equal, we don't want it, so recurse
            remove_duplicates(head.next_node, seen_head)
            return

        else: # head.data > seen_node.data
            # continue while loop
            prev_seen = seen_node
            seen_node = seen_node.next_node

    # if I get through the while loop without returning, what does that mean?
    # it means head belongs at the end of the list
    # and still have to consider no prev_seen
    next_head = head.next_node
    head.next_node = None

    if not prev_seen:
        # here prev_seen and seen_node are None
        # so basically this is just the first time through
        seen_head = head
    else:
        prev_seen.next_node = head


    # then recurse and return
    remove_duplicates(next_head, seen_head)
    return