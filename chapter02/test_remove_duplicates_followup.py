from unittest import TestCase
from q01_followup import remove_duplicates
from linked_list import Node
from copy import copy

__author__ = 'Scott'


class TestRemove_duplicates_followup(TestCase):
    def setUp(self):
        """
        gets called before every test
        :return:
        """
        self.list1 = Node(1)
        self.list1.append(Node(2))

        self.list2 = copy(self.list1)
        self.list2.append(Node(1))

        return

    def test_setup(self):
        assert self.list1 != self.list2

    def test_remove_duplicates(self):
        remove_duplicates(self.list2)
        assert self.list1 == self.list2