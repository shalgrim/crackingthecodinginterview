import unittest
from unittest import TestCase
from linked_list import Node

__author__ = 'Scott'


class TestNode(TestCase):

    def setUp(self):
        self.one_node_list = Node(1)
        self.two_node_list = Node(2)
        self.two_node_list.append(Node(3))

    def test_node_create_1(self):
        self.assertEqual(self.one_node_list.data, 1)

    def test_node_create_2(self):
        self.assertIsNone(self.one_node_list.next_node)

    def test_list_create_1(self):
        self.assertEqual(self.two_node_list.data, 2)

    def test_list_create_2(self):
        self.assertIsNotNone(self.two_node_list.next_node)

    def test_list_create_3(self):
        self.assertEqual(self.two_node_list.next_node.data, 3)

    def test_list_create_4(self):
        self.assertIsNone(self.two_node_list.next_node.next_node)

    def test_append_1(self):
        new_node = Node(4)
        self.one_node_list.append(new_node)
        self.assertEqual(self.one_node_list.next_node, new_node)

if __name__ == '__main__':
    unittest.main()