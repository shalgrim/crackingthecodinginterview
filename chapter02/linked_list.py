"""
linked_list.py
8/18/14
Basic implementation to be used for chapter two questions in cracking the
coding interview
"""
__author__ = 'Scott'

class Node:
    """
    Node in a linked list
    """
    data = None
    next_node = None

    def __init__(self, d):
        """
        Constructor
        :param d: data element
        :return: new Node with d as data
        """
        self.data = d

    def append(self, new_node):
        """
        appends new_node to the end of this list
        :param new_node: a Node to be appended to the end of this list
        :return: none
        """

        if self.next_node:
            self.next_node.append(new_node)
        else:
            self.next_node = new_node

    def __eq__(self, other):
        return self.data == other.data and self.next_node == other.next_node

