/**
 * Created by Scott on 6/12/2014.
 */
public class Q1_01 {
    public static boolean hasAllUniqueChars(String s) {
        boolean[] charsSeen = new boolean[256];

        // assume default is false;

        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if (charsSeen[(int)c])
                return false;
            else
                charsSeen[(int)c] = true;
        }

        return true;
    }
}
