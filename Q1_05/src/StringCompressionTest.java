/**
 * Created by Scott on 6/29/2014.
 */

import org.junit.*;
import static org.junit.Assert.*;

public class StringCompressionTest {
    @Test
    public void testExample()
    {
        String input = "aabcccccaaa";
        String expected = "a2b1c5a3";
        String actual = StringCompression.compressString(input);
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testTooShort()
    {
        String input = "aa";
        assertTrue(input.equals(StringCompression.compressString(input)));
    }

    @Test
    public void testJustLongEnough()
    {
        assertTrue(StringCompression.compressString("aaa").equals("a3"));
    }

    @Test
    public void testLongEnoughButNotEnoughRepeats01()
    {
        assertTrue(StringCompression.compressString("aab").equals("aab"));
    }

    @Test
    public void testLongEnoughButNotEnoughRepeats02()
    {
        assertTrue(StringCompression.compressString("abc").equals("abc"));
    }

    @Test
    public void testCompressOneCharacter01()
    {
        assertTrue(StringCompression.compressString("aabbccddd").equals
                ("a2b2c2d3"));
    }

    @Test
    public void testCompressOneCharacter02()
    {
        assertTrue(StringCompression.compressString("aaabbccdd").equals
                ("a3b2c2d2"));
    }

    @Test
    public void testCompressMultiDigits()
    {
        assertTrue(StringCompression.compressString("aaaaaaaaaa").equals
                ("a10"));
    }

    @Test
    public void testMultiDigitsAndOneChar01()
    {
        assertTrue(StringCompression.compressString("aaaaaaaaaabcdefg")
                .equals("a10b1c1d1e1f1g1"));
    }

    @Test
    public void testMultiDigitsAndOneChar02()
    {
        assertTrue(StringCompression.compressString("abcdefghhhhhhhhhhh")
                .equals("a1b1c1d1e1f1g1h11"));
    }

    @Test
    public void testMultiDigitsAndTooLong()
    {
        // test length "a1b1c1d1e1f1g1h10";
        String input = "abcdefghhhhhhhhhh";
        String expected = input;
        String actual = StringCompression.compressString(input);
        assertTrue(actual.equals(expected));
    }
}
