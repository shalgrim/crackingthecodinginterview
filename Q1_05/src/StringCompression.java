/**
 * Created by Scott on 6/28/2014.
 */
public class StringCompression {

    /*
     * Implement a method to perform basic string compression using the
     * counts of repeated characters. For example, the string aabcccccaaa
     * would become a2b1c5a3. If the "compressed" string would not become
     * smaller than the original string, your method should return the
     * original string. You can assume the string has only upper and lower
     * case letters (a-z).
     */

    public static String compressString(String input)
    {
        int inlen = input.length();
        if (inlen < 3)
            return input;

        int outlen = inlen - 1;
        char[] outArray = new char[outlen];
        int outInd = 0;
        outArray[outInd] = input.charAt(0);
        int count = 1;

        for (int inInd = 1; inInd < input.length(); inInd++)
        {
            if (input.charAt(inInd) == outArray[outInd])
                count++;
            else {
                char[] countChars = Integer.toString(count).toCharArray();
                int countLen = countChars.length;

                // the +2 is because you'll have at least two more chars
                // after this for the next alpha char plus its count of at
                // least one
                if (outInd + countLen + 2 >= outlen)
                    return input;

                outInd = appendCountToCompressedString(outArray, outInd, countChars, countLen);
                outArray[++outInd] = input.charAt(inInd);
                count = 1;
            }
        }

        // Add the last count to the string
        char[] countChars = Integer.toString(count).toCharArray();
        int countLen = countChars.length;
        if (outInd + countLen >= outlen)
            return input;
        outInd = appendCountToCompressedString(outArray, outInd, countChars, countLen);

        // create new array exactly the right length and fill that up to be
        // final output
        char[] finalArray = new char[outInd+1];
        for (int i = 0; i < finalArray.length; i++)
            finalArray[i] = outArray[i];
        return new String(finalArray);
    }

    private static int appendCountToCompressedString(char[] outArray, int outInd, char[] countChars, int countLen) {
        for (int i = 0; i < countLen; i++)
            outArray[++outInd] = countChars[i];
        return outInd;
    }
}
