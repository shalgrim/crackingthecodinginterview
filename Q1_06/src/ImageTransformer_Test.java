/**
 * Created by Scott on 7/2/2014.
 */

import org.junit.*;
import static org.junit.Assert.*;

public class ImageTransformer_Test {
    int[][] inImage0 = {{}};
    int[][] inImage1 = {{1}};
    int[][] inImage2 = {{1, 2}, {3, 4}};
    int[][] inImage3 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    int[][] inImage4 = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12},
            {13, 14, 15, 16}};
    int[][] outImage0 = {{}};
    int[][] outImage1 = {{1}};
    int[][] outImage2 = {{3, 1},{4, 2}};
    int[][] outImage3 = {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}};
    int[][] outImage4 = {{13, 9, 5, 1}, {14, 10, 6, 2}, {15, 11, 7, 3},
            {16, 12, 8, 4}};
    int[][] errImage1 = {{1}, {2, 3}};
    int[][] errImage2 = {{1, 2}, {3}};

    @Test
    public void testImage0()
    {
        ImageTransformer.rotateImage90Clockwise(inImage0);
        assertArrayEquals(outImage0, inImage0);
    }

    @Test
    public void testImage1()
    {
        ImageTransformer.rotateImage90Clockwise(inImage1);
        assertArrayEquals(outImage1, inImage1);
    }
    
    @Test
    public void testImage2()
    {
        ImageTransformer.rotateImage90Clockwise(inImage2);
        assertArrayEquals(outImage2, inImage2);
    }
    
    @Test
    public void testImage3()
    {
        ImageTransformer.rotateImage90Clockwise(inImage3);
        assertArrayEquals(outImage3, inImage3);
    }
    
    @Test
    public void testImage4()
    {
        ImageTransformer.rotateImage90Clockwise(inImage4);
        assertArrayEquals(outImage4, inImage4);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testErrImage1()
    {
        ImageTransformer.rotateImage90Clockwise(errImage1);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testErrImage2()
    {
        ImageTransformer.rotateImage90Clockwise(errImage2);
    }
}
