/**
 * Created by Scott on 7/2/2014.
 */
public class ImageTransformer {
    public static void rotateImage90Clockwise(int[][] image)
    {
        int N = image.length;
        if (N == 0 || N == 1)
            return; // no changes necessary

        for (int ring = 0; ring < (N+1)/2; ring++)
        {
            for (int i = ring; i < N-1-ring; i++)
            {
                int store = image[ring][i];                 // store top

                // move left to top
                image[ring][i] = image[N-1-i][ring];

                // move bottom to left
                image[N-1-i][ring] = image[N-1-ring][N-1-i];

                // move right to bottom
                image[N-1-ring][N-1-i] = image[i][N-1-ring];
                image[i][N-1-ring] = store; // move top to right
            }
        }

        return;
    }
}
