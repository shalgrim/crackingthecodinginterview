import java.io.IOException;

/**
 * Created by Scott on 6/22/2014.
 * Write a method to replace all spaces in a string with '%20'. You may
 * assume that the string has sufficient space at the end of the string to hold
 * the additional characters and that you are given the "true" length of the
 * string. (Note: if implementing in Java, please use a character array so that
 * you can perform this operation in place.
 */
public class Q1_04 {
    public static void main(String[] args) {

        String input;
        StringBuilder sb = new StringBuilder("");

        for (int a = 0; a < args.length; a++)
        {
            if (a > 0)
                sb.append(' ');
            sb.append(args[a]);
        }

        input = sb.toString();
        char[] chars = prepInput(input);

        convertSpaces(chars, input.length());
        System.out.println(String.format("Input string: %s", input));
        System.out.println(String.format("Output: %s", new String(chars)));
        System.out.println("Press any key to continue...");

        try {
            System.in.read();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }
    }

    static char[] prepInput(String input) {
        int numSpaces = 0;
        int startInd = 0;

        while (input.substring(startInd).indexOf(' ') != -1)
        {
            numSpaces++;
            startInd += input.substring(startInd).indexOf(' ') + 1;
        }

        char[] chars = new char[input.length() + numSpaces*2];

        for (int i = 0; i < input.length(); i++)
        {
            chars[i] = input.charAt(i);
        }
        return chars;
    }

    static void convertSpaces(char[] s, int trueLen)
    {
        // count spaces in string
        int numSpaces = 0;

        for (int i = 0; i < trueLen; i++)
        {
            if (s[i] == ' ')
                numSpaces++;
        }

        // walk backward, copying chars and converting spaces
        int j = trueLen-1;
        int k = j + 2*numSpaces;

        // base case is 0 spaces so never enter loop
        while (j < k)
        {
            if (s[j] == ' ') {
                s[k--] = '0';
                s[k--] = '2';
                s[k--] = '%';
                j--;
            } else {
                s[k--] = s[j--];
            }
        }
    }
}
