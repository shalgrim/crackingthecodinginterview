/**
 * Created by Scott on 6/22/2014.
 */

import org.junit.*;
import static org.junit.Assert.*;

public class Q1_04Test {

    @Test
    public void testPrepInput()
    {
        char[] expected = {'M', 'r', ' ', 'J', 'o', 'h', 'n', ' ', 'S', 'm',
                'i', 't', 'h', '\0', '\0', '\0', '\0'};
        char[] actual = Q1_04.prepInput("Mr John Smith");
        assertArrayEquals(expected, actual);
    }

    @Test
    public void bookExample() {
        String myString = "Mr John Smith";
        char[] input = Q1_04.prepInput(myString);
        char[] output = {'M', 'r', '%', '2', '0', 'J', 'o', 'h', 'n', '%',
                '2', '0', 'S', 'm', 'i', 't', 'h'};
        Q1_04.convertSpaces(input, myString.length());
        assertArrayEquals(output, input);
    }

    @Test
    public void noSpaces() {
        String myString = "MrJohnSmith";
        char[] input = Q1_04.prepInput(myString);
        char[] output = Q1_04.prepInput(myString);
        Q1_04.convertSpaces(input, myString.length());
        assertArrayEquals(output, input);
    }

    @Test
    public void startsWithSpace() {
        String myString = " Mr John Smith";
        char[] input = Q1_04.prepInput(myString);
        char[] output = {'%', '2', '0', 'M', 'r', '%', '2', '0', 'J', 'o',
                'h', 'n', '%', '2', '0', 'S', 'm', 'i', 't', 'h'};
        Q1_04.convertSpaces(input, myString.length());
        assertArrayEquals(output, input);
    }

    @Test
    public void multiSpaces() {
        String myString = " Mr  John Smith";
        char[] input = Q1_04.prepInput(myString);
        char[] output = {'%', '2', '0', 'M', 'r', '%', '2', '0', '%', '2',
                '0', 'J', 'o', 'h', 'n', '%', '2', '0', 'S', 'm', 'i', 't',
                'h'};
        Q1_04.convertSpaces(input, myString.length());
        assertArrayEquals(output, input);
    }
}
