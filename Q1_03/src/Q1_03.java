import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Scott on 6/17/2014.
 * Given two strings, write a method to decide if one is a permutation of each other
 */
public class Q1_03 {
    public static void main(String[] args) {

        if (args.length != 2) {
            System.err.println("Usage: java Q1_03 s1 s2");
            return;
        }

        String s1, s2, notstring;
        s1 = args[0];
        s2 = args[1];

        if (arePermutes(s1, s2)){
            notstring = "";
        } else
        {
            notstring = " not";
        }

        System.out.println(String.format("%s and %s are%s permutes of each other", s1, s2, notstring));
        try {
            System.in.read(); // is there a better way to pause in Java
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        return;
    }

    static boolean arePermutes(String s1, String s2) {

        if (s1.length() != s2.length())
        {
            return false;
        }

        HashMap<Character, Integer> charCounter = new HashMap<Character, Integer>();

        for (int i = 0; i < s1.length(); i++) {
            char key = s1.charAt(i);

            if (charCounter.containsKey(key))
                charCounter.put(key, charCounter.get(key)+1);
            else
                charCounter.put(key, 1);
        }

        for (int j = 0; j < s2.length(); j++)
        {
            char c = s2.charAt(j);

            // TODO: check precedence. design tests to exploit
            if (!charCounter.containsKey(c) || charCounter.get(c) == 0)
                return false;

            charCounter.put(c, charCounter.get(c)-1);
        }

        return true;
    }
}
