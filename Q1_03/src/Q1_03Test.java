/**
 * Created by Scott on 6/18/2014.
 */

import org.junit.*;
import static org.junit.Assert.*;

public class Q1_03Test {

    @Test
    public void UnequalStringLengths()
    {
        assertFalse(Q1_03.arePermutes("a", "bc"));
    }

    @Test
    public void NewCharInS2() {
        assertFalse(Q1_03.arePermutes("abc", "dab"));
    }

    @Test
    public void Subset1() {
        assertFalse(Q1_03.arePermutes("abc","acc"));
    }

    @Test
    public void Subset2() {
        assertFalse(Q1_03.arePermutes("acc","abc"));
    }

    @Test
    public void Permutes() {
        assertTrue(Q1_03.arePermutes("The quick brown fox","fox The quick wrobn"));
    }

}
