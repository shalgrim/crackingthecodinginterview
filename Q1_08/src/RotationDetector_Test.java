/**
 * Created by Scott on 7/13/2014.
 */

import org.junit.*;
import static org.junit.Assert.*;

public class RotationDetector_Test {

    @Test
    public void testExample() {
        assertTrue(RotationDetector.isRotation("waterbottle", "erbottlewat"));
    }

    @Test
    public void testFalseLength() {
        assertFalse(RotationDetector.isRotation("bottle", "waterbottle"));
    }

    @Test
    public void testFalse() {
        assertFalse(RotationDetector.isRotation("waterbottle", "aterbottlex"));
    }

    @Test
    public void testSame() {
        assertTrue(RotationDetector.isRotation("waterbottle", "waterbottle"));
    }

    @Test
    public void testEmpties() {
        assertTrue(RotationDetector.isRotation("", ""));
    }
}
