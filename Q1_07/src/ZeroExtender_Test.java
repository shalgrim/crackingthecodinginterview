/**
 * Created by Scott on 7/6/2014.
 */

import org.junit.*;

import java.io.*;

import static org.junit.Assert.*;

public class ZeroExtender_Test {
    int[][] nullMatrix;
    int[][] emptyMatrixInput = {};
    int[][] emptyMatrixOutput = {};
    int[][] nonZeroMatrixInput = {{1, 2, 3}, {4, 5, 6}};
    int[][] someZeroesMatrixInput = {{1, 0, 2}, {3, 4, 5}, {0, 6, 7}};
    int[][] nonZeroMatrixOutput = {{1, 2, 3}, {4, 5, 6}};
    int[][] someZeroesMatrixOutput = {{0, 0, 0}, {0, 0, 5}, {0, 0, 0}};
    int[][] unevenMatrix1 = {{1, 2, 3}, {4, 5}};
    int[][] unevenMatrix2 = {{1, 2}, {3, 4, 5}};

    // stuff for reading testing stderr
    private PipedInputStream pin;
    private PipedOutputStream pout;
    private PrintStream myErr;
    private BufferedReader br;

    @Before
    public void setUp() throws IOException
    {
        pin = new PipedInputStream();
        pout = new PipedOutputStream(pin);
        myErr = new PrintStream(pout);
        System.setErr(myErr);
        br = new BufferedReader(new InputStreamReader(pin));
    }

    @After
    public void tearDown(){
        System.setErr(System.err);
    }

    @Test
    public void testNullMatrix()
    {
        ZeroExtender.extendZeroes(nullMatrix);
        assertNull(nullMatrix);
    }

    @Test
    public void testNullMatrixStderr() throws IOException {
        String expected = "Received empty matrix";

        ZeroExtender.extendZeroes(nullMatrix);

        myErr.flush(); // to prevent waiting?
        String actual = br.readLine();
        assertEquals(expected, actual);
    }

    @Test
    public void testEmptyMatrix()
    {
        ZeroExtender.extendZeroes(emptyMatrixInput);
        assertArrayEquals(emptyMatrixInput, emptyMatrixOutput);
    }

    @Test
    public void testNonZeroMatrix()
    {
        ZeroExtender.extendZeroes(nonZeroMatrixInput);
        assertArrayEquals(nonZeroMatrixInput, nonZeroMatrixOutput);
    }

    @Test
    public void testSomeZeroesMatrix() {
        ZeroExtender.extendZeroes(someZeroesMatrixInput);
        assertArrayEquals(someZeroesMatrixInput, someZeroesMatrixOutput);
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testUnevenMatrix1Exception() {
        ZeroExtender.extendZeroes(unevenMatrix1);
    }

    public void testUnevenMatrix1Stderr() throws IOException {
        String expected = "give all rows in matrix equal length";

        try {
            ZeroExtender.extendZeroes(unevenMatrix1);
        } catch (UnsupportedOperationException uoe) {
            assertEquals(expected, uoe.getMessage());
        }
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testUnevenMatrix2Exception() {
        ZeroExtender.extendZeroes(unevenMatrix2);
    }

    @Test
    public void testUnevenMatrix2Stderr() throws IOException {
        String expected = "give all rows in matrix equal length";

        try {
            ZeroExtender.extendZeroes(unevenMatrix2);
        } catch (UnsupportedOperationException uoe) {
            assertEquals(expected, uoe.getMessage());
        }
    }
}
