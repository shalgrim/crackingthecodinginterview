import java.util.HashSet;

/**
 * Created by Scott on 7/6/2014.
 * Write an algorithm such that if an element in an M x N matrix is 0,
 * its entire row and column are 0.
 */
public class ZeroExtender {
    public static void extendZeroes(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            System.err.println("Received empty matrix");
            return;
        }

        int M = matrix.length;
        int N = matrix[0].length;
        HashSet<Integer> rowsToZero = new HashSet<>();
        HashSet<Integer> columnsToZero = new HashSet<>();
        String unequalError = "give all rows in matrix equal length";

        // first check each cell, note rows and and columns to zero
        for (int i = 0; i < M; i++) {

            if (matrix[i].length != N) {
                throw new UnsupportedOperationException(unequalError);
            }

            for (int j = 0; j < N; j++) {
                if (matrix[i][j] == 0) {
                    rowsToZero.add(i);
                    columnsToZero.add(j);
                }
            }
        }

        // first zero out rows
        for (int i : rowsToZero) {
            for (int j = 0; j < N; j++)
                matrix[i][j] = 0;
        }

        // now do the columns
        for (int j : columnsToZero) {
            for (int i = 0; i < M; i++)
                matrix[i][j] = 0;
        }
    }
}
